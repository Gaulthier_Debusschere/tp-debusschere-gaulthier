
function Fibonacci(prmNbFibonacci) {

    // variable de type TABLEAU 
    let tabFib = [];

    if (prmNbFibonacci >= 0) {
        tabFib.push(0);
        // tabFib contient [0]
    }

    if (prmNbFibonacci >= 1) {
        tabFib.push(1);
        // tabFib contient [0, 1]
    }

    if (prmNbFibonacci >= 2)  {
        //On y retrouve les autres termes suivants de la suite
        // et on les stockes dans le tableau
        for (let i = 2; i < prmNbFibonacci; i++) {
            tabFib.push(tabFib[i-2] + tabFib[i-1]);
        }
    }

    //Ici, on retourne le tableau qui contient les termes de la suite
    return tabFib;
}

//Ici, on appel la fonction pour calculer les termes de Fibonacci
let tabFibonacci = Fibonacci(17);

//affichage du contenu du tableau en parcourant chacun de ses éléments
let affichage = "Suite de Fibonacci (17 premiers termes) :\n";
for (let i = 0; i< tabFibonacci.length;i++){
    affichage += tabFibonacci[i] + "-";
}

//On supprimer le dernier caractère de la String affichage car il y a un tiret "-" en trop

affichage = affichage.slice(0, -1);

//affichage
alert(affichage);