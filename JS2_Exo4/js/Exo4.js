$(document).ready(function () {

    //variable qui enregistre l'état affichée / cachée de la DIV
    let divAffichee = false; //au démarrage la div est cachée

 
    //Fonction évenementielle de gestion du CLIC sur le bouton #btn1
    $("#btn1").click(function () {

        if (divAffichee === false) {
            //pour afficher la DIV
            $("#maDiv1").show("slow");
            $("#btn1").attr("value", "Fermer");
        } else {
            //pour cacher la DIV
            $("#maDiv1").hide("slow");
            $("#btn1").attr("value", "Ouvrir");
        }
        //modifier la valeur de la variable d'état pour correspondre à l'état actuel
        //le symbole ! est un opérateur de "négation" qui inverse l'état d'un booléen
        divAffichee = !divAffichee;

    });

});