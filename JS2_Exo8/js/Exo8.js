/*Debusschere Gaulthier
Fait le 28/05/2020
Scrip JS v1
Permet d'ajouter une colonne d'un tableau et d'y calculer la somme 

*/
$(document).ready(function () {                                                             //attend le chargement du document



    $("#btnAjout").click(                                                                   //evenement clique sur le bouton Ajouter
        function () {
            let longueurtableau = 1 + $("table tbody td").length;                           //calcule la longueur du tableau
            let sommerecu = calculTotal(longueurtableau);                                   //Appelle de la fonction qui calcul la somme

            $("table tbody").append("<tr><td>" + longueurtableau + "</td></tr>");           //ajout d'une case avec le nombre de ligne de fin de tableau
            $("table tfoot tr").html("<td>" + sommerecu + "</td>");                         //modifi la somme du bas de tableau
        }
    );



    $("#btnSupp").click(                                                                         //evenement clique sur le bouton Supprimer
        function () {
            $("table tbody tr td:last").remove();                                               //effacement de la derniere ligne du tableau
            let longueurtableau = $("table tbody td").length; //calcul de la longueur du tableau pas de +1 pour ne pas faire de decalage a cause du symbole (<=) dans la fonction de somme
            let sommerecu = calculTotal(longueurtableau);                                       //appelle de la fonction de calcul de la somme

            $("table tfoot tr").replaceWith("<tr><td>" + sommerecu + "</td></tr>");             //remplacement de la valeur de la somme
        }
    );



    function calculTotal(prmlongueur) {                                                          //Fonction de calcul de la somme
        let somme = 0;                                                                           //initialisation d'une valeur de retour
        for (let i = 0; i <= prmlongueur; i++) {                                                 //boucle de calcul de somme a chaque pas tant que i <= a la longueur du tableau
            somme = somme + i;
        }
        return somme;                                                                            //retour de valeur
    }


});
