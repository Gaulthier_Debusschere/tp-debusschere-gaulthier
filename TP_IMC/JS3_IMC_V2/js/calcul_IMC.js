// déclaration des variables
let Poids = "";
let Taille = "";
let IMC = "";
let interpretation = "";

$("#btnCalcul").click(                              // click sur le bouton "btnCalcul"
    function () {

        Poids = $("#Poids").val();              // selectionne la valeur de la zone de saisie du poids
        Taille = $("#Taille").val();            // selectionne la valeur de la zone de saisie de la Taille

        // vérification saisie
        if (isNaN(Poids) || isNaN(Taille)) {        // si un champ contient autre chose qu'un chiffre
            alert("Erreur de saisie")               // message d'erreur
        } else {

            // règles à respecter pour les varaibles
            Poids = Poids.replace(",", ".");            // remplacement des "," par des "."
            Poids = Number(Poids);                      // converti en nombre

            Taille = Taille.replace(",", ".");          // remplacement des "," par des "."
            Taille = Number(Taille);                    // converti en nombre

            // affiche le résultat de la fonction CalculerIMC
            IMC = CalculerIMC(Poids, Taille);                                   // calcul de l'IMC
            interpretation = InterpreterIMC(IMC);                               // interpretation de l'IMC                            
            $("#result").html(IMC + " (" + interpretation + ")");             // affichage du résultat et l'interprétation
        }
    }
);

function CalculerIMC(prmPoids, prmTaille) {         // déclaration de la fonction Calculer IMC
    let res = prmPoids / (prmTaille * prmTaille);   // calcul de l'IMC
    return res.toFixed(1);                          // retourne le résultat
}

function InterpreterIMC(prmIMC) {                   // déclaration de la fonction InterpreterIMC
    let inter = "";                                 // variable qui prend le message

    switch (true) {                                 // selon les valeurs de l'IMC
        case (prmIMC < 16.5):                       // < à 16.5
            inter = "dénutrition";                  // dénutrition sera inscrit 
            break;
        case (prmIMC >= 16.5 && prmIMC < 18.5):     // compris entre 16.5 et 18.5
            inter = "maigreur";                     //maigreur sera inscrit 
            break;
        case (prmIMC >= 18.5 && prmIMC < 25):       // compris entre 18.5 et 25
            inter = "corpulence normale";           // corpulence normale sera inscrit 
            break;
        case (prmIMC >= 25 && prmIMC < 30):         // compris entre 25 et 30
            inter = "surpoids";                     // surpoids sera inscrit 
            break;
        case (prmIMC >= 30 && prmIMC < 35):         // compris entre 30 et 35
            inter = "obésité modérée";              // obésité modérée sera inscrit 
            break;
        case (prmIMC >= 35 && prmIMC < 40):         // compris entre 35 et 40
            inter = "obésité sévère";               // obésité sévère sera inscrit 
            break;
        case (prmIMC >= 40):                        // > à 40
            inter = "obésité morbide";              // obésité morbide sera inscrit 
            break;
        default:                                    // par défaut
            inter = "erreur";                       // message d'erreur
    }
    return inter;                                   // retourne l'interprétation
}